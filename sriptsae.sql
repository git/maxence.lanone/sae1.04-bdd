DROP TABLE PARAM;
DROP TABLE FOURNIR;
DROP TABLE COMMANDE;
DROP TABLE CLIENTS;
DROP TABLE FOURNISSEURS;
DROP TABLE PRODUITS;


CREATE TABLE PARAM(
nomEntreprise varchar(30),
numTel numeric,
email varchar(100),
pays varchar(30),
ville varchar(30),
rue varchar(100)
);


CREATE TABLE FOURNISSEURS(
numFournisseur char(8) PRIMARY KEY,
nom varchar(50) NOT NULL UNIQUE,
pays varchar(30) NOT NULL,
ville varchar(50) NOT NULL,
codePostal varchar(10) NOT NULL,
nomRue varchar(50) NOT NULL, 
numTel numeric (10) NOT NULL UNIQUE,
email varchar(50) NOT NULL UNIQUE,
UNIQUE(ville, codePostal, nomRue)
);

CREATE TABLE PRODUITS(
numProd char(8) PRIMARY KEY,
typeVet varchar(30)NOT NULL CHECK(typeVet IN ('casquette','t-shirt','pull','veste','masque','bonnet','pantalon')),
nom varchar(30) NOT NULL,
stock numeric CHECK(stock >= 0),
prixUnitaire numeric NOT NULL CHECK(prixUnitaire > 0),
taille varchar(30)NOT NULL CHECK (taille IN ('XS','S','M','L','XL','XXL','unique')),
compoPrincipal varchar(30) NOT NULL CHECK(compoPrincipal IN ('coton','polyester','laine','velours')),
couleursDispo varchar(200) NOT NULL,
CHECK((typeVet IN('casquette','bonnet','masque') AND taille IN ('unique'))OR typeVet IN('veste','t-shirt','pull','pantalon') AND taille!='unique') 
);

CREATE TABLE CLIENTS(
numClient char(8) PRIMARY KEY,
nom varchar(30) NOT NULL,
prenom varchar(30) NOT NULL,
pays varchar(30) NOT NULL,
ville varchar(50) NOT NULL,
codePostal varchar(20) NOT NULL DEFAULT '63000',
nomRue varchar(50) NOT NULL,
numTel varchar(15) NOT NULL UNIQUE,
email varchar(50) NOT NULL UNIQUE,
UNIQUE(ville, codePostal, nomRue)
);

CREATE TABLE COMMANDE(
numClient char(8) REFERENCES CLIENTS(numClient),
numProd char(8) REFERENCES PRODUITS(numProd),
numCommande char(8) PRIMARY KEY CHECK(numCommande LIKE 'C%'),
prix_total numeric NOT NULL CHECK(prix_total > 0),
qte numeric NOT NULL CHECK(qte > 0),
dateCommande date NOT NULL --CHECK(dateCommande <= CURRENT_DATE)
);


CREATE TABLE FOURNIR(
numProd char(8) REFERENCES PRODUITS,
numFournisseur char(8) REFERENCES FOURNISSEURS,
prixUFournisseur numeric NOT NULL CHECK (prixUFournisseur > 0.00),
qte numeric NOT NULL CHECK(qte > 0),
PRIMARY KEY(numProd,numFournisseur)
);



--Insert PARAM

INSERT INTO PARAM VALUES('Cunong',0611037832,'CunongCoOfficial@gmail.com','France','Clermont-Ferrand','12 rue des Meuniers');

-- INSERT PRODUIT



-- Insert Casquettes

INSERT INTO PRODUITS VALUES('CASQ0001','casquette','Kasket',100,100,'unique','polyester','vert_de_vessie');
INSERT INTO PRODUITS VALUES('CASQ0002','casquette','Kasket',100,100,'unique','polyester','bleu_pomme');
INSERT INTO PRODUITS VALUES('CASQ0003','casquette','Keps',100,150,'unique','velours','jaune_moutarde');
INSERT INTO PRODUITS VALUES('CASQ0004','casquette','Keps',100,150,'unique','velours','noir');
INSERT INTO PRODUITS VALUES('CASQ0005','casquette','Cap',100,120,'unique','coton','cuisse_de_nymphe');
INSERT INTO PRODUITS VALUES('CASQ0006','casquette','Cap',100,120,'unique','coton','queue_de_renard');
INSERT INTO PRODUITS VALUES('CASQ0007','casquette','Mütsike',100,140,'unique','laine','fraise_écrasée');
INSERT INTO PRODUITS VALUES('CASQ0008','casquette','Mütsike',100,140,'unique','laine','poil_de_chameau');

-- Insert T-shirts

INSERT INTO PRODUITS VALUES('TSHI0001','t-shirt','T-särk',100,250,'XL','polyester','vert_de_vessie');
INSERT INTO PRODUITS VALUES('TSHI0002','t-shirt','T-särk',100,250,'XL','polyester','bleu_pomme');
INSERT INTO PRODUITS VALUES('TSHI0003','t-shirt','T-särk',100,300,'S','velours','blanc');
INSERT INTO PRODUITS VALUES('TSHI0004','t-shirt','T-särk',100,300,'S','velours','noir');
INSERT INTO PRODUITS VALUES('TSHI0005','t-shirt','Camiseta',100,290,'L','coton','cuisse_de_nymphe');
INSERT INTO PRODUITS VALUES('TSHI0006','t-shirt','Camiseta',100,290,'L','coton','queue_de_renard');
INSERT INTO PRODUITS VALUES('TSHI0007','t-shirt','Ikea',100,260,'M','laine','poil_de_chameau');
INSERT INTO PRODUITS VALUES('TSHI0008','t-shirt','Ikea',100,260,'M','laine','fraise_écrasée');


-- Insert Pulls

INSERT INTO PRODUITS VALUES('PULL0001','pull','Sweater',100,500,'XL','polyester','vert_de_vessie');
INSERT INTO PRODUITS VALUES('PULL0002','pull','Sweater',100,500,'XL','polyester','bleu_pomme');
INSERT INTO PRODUITS VALUES('PULL0003','pull','Sutra',100,550,'S','velours','jaune');
INSERT INTO PRODUITS VALUES('PULL0004','pull','Sutra',100,550,'S','velours','bleu');
INSERT INTO PRODUITS VALUES('PULL0005','pull','Agasalho',100,520,'M','coton','cuisse_de_nymphe');
INSERT INTO PRODUITS VALUES('PULL0006','pull','Agasalho',100,520,'M','coton','queue_de_renard');
INSERT INTO PRODUITS VALUES('PULL0007','pull','Sueter',100,580,'L','laine','poil_de_chameau');
INSERT INTO PRODUITS VALUES('PULL0008','pull','Sueter',100,580,'L','laine','fraise_écrasée');

-- Insert Vestes

INSERT INTO PRODUITS VALUES('VEST0001','veste','Jacket',100,800,'XL','coton','vert_de_vessie');
INSERT INTO PRODUITS VALUES('VEST0002','veste','Jacket',100,800,'XL','coton','bleu_pomme');
INSERT INTO PRODUITS VALUES('VEST0003','veste','Alsutra',100,950,'S','velours','bleu');
INSERT INTO PRODUITS VALUES('VEST0004','veste','Alsutra',100,950,'S','velours','noir');
INSERT INTO PRODUITS VALUES('VEST0005','veste','Jaqueta',100,720,'M','polyester','queue_de_renard');
INSERT INTO PRODUITS VALUES('VEST0006','veste','Jaqueta',100,720,'M','polyester','cuisse_de_nymphe');
INSERT INTO PRODUITS VALUES('VEST0007','veste','Chaqueta',100,799,'L','laine','poil_de_chameau');
INSERT INTO PRODUITS VALUES('VEST0008','veste','Chaqueta',100,799,'L','laine','fraise_écrasée');

-- Insert Masques

INSERT INTO PRODUITS VALUES('MASQ0001','masque','Mask',100,75,'unique','coton','bleu_pomme');
INSERT INTO PRODUITS VALUES('MASQ0002','masque','Mask',100,75,'unique','coton','vert_de_vessie');
INSERT INTO PRODUITS VALUES('MASQ0003','masque','Qinae',100,50,'unique','velours','marron');
INSERT INTO PRODUITS VALUES('MASQ0004','masque','Qinae',100,50,'unique','velours','jaune');
INSERT INTO PRODUITS VALUES('MASQ0005','masque','Mascarar',100,50,'unique','polyester','queue_de_renard');
INSERT INTO PRODUITS VALUES('MASQ0006','masque','Mascarar',100,50,'unique','polyester','cuisse_de_nymphe');
INSERT INTO PRODUITS VALUES('MASQ0007','masque','Maskë',100,50,'unique','laine','poil_de_chameau');
INSERT INTO PRODUITS VALUES('MASQ0008','masque','Maskë',100,50,'unique','laine','fraise_écrasée');

-- Insert Bonnets

INSERT INTO PRODUITS VALUES('BONN0001','bonnet','Cap',100,150,'unique','coton','bleu_pomme');
INSERT INTO PRODUITS VALUES('BONN0002','bonnet','Cap',100,150,'unique','coton','vert_de_vessie');
INSERT INTO PRODUITS VALUES('BONN0003','bonnet','Qubea',100,170,'unique','velours','bleu');
INSERT INTO PRODUITS VALUES('BONN0004','bonnet','Qubea',100,170,'unique','velours','noir');
INSERT INTO PRODUITS VALUES('BONN0005','bonnet','Gorra',100,120,'unique','polyester','cuisse_de_nymphe');
INSERT INTO PRODUITS VALUES('BONN0006','bonnet','Gorra',100,120,'unique','polyester','queue_de_renard');
INSERT INTO PRODUITS VALUES('BONN0007','bonnet','Kapak',100,170,'unique','laine','poil_de_chameau');
INSERT INTO PRODUITS VALUES('BONN0008','bonnet','Kapak',100,170,'unique','laine','fraise_écrasée');

-- Pantalons

INSERT INTO PRODUITS VALUES('PANT0001','pantalon','Trousers',100,690,'XL','coton','bleu_pomme');
INSERT INTO PRODUITS VALUES('PANT0002','pantalon','Trousers',100,690,'XL','coton','vert_de_vessie');
INSERT INTO PRODUITS VALUES('PANT0003','pantalon','Pantalones',100,750,'S','velours','jaune');
INSERT INTO PRODUITS VALUES('PANT0004','pantalon','Pantalones',100,750,'S','velours','bleu');
INSERT INTO PRODUITS VALUES('PANT0005','pantalon','Hose',100,600,'M','polyester','queue_de_renard');
INSERT INTO PRODUITS VALUES('PANT0006','pantalon','Hose',100,600,'M','polyester','cuisse_de_nymphe');
INSERT INTO PRODUITS VALUES('PANT0007','pantalon','Pantaloni',100,700,'L','laine','poil_de_chameau');
INSERT INTO PRODUITS VALUES('PANT0008','pantalon','Pantaloni',100,700,'L','laine','fraise_écrasée');




-- INSERT FOURNISSEURS

INSERT INTO FOURNISSEURS VALUES('FOUR0001', 'Utopia', 'Etats-Unis', 'New-York', '63000', '113 MacDougal Street', '0742856510', 'utopia63000@gmail.com'); --coton
INSERT INTO FOURNISSEURS VALUES('FOUR0002', 'Luigi', 'Allemagne', 'Berlin', '10115', ' Matthäikirchplatz 6', '0452786341', 'luigi-mario@gmail.com'); --polyester
INSERT INTO FOURNISSEURS VALUES('FOUR0003', 'Paladin', 'France', 'Paris', '75000', 'Avenue des Champs-Élysées', '0685413296', 'paladimuskarpone@gmail.com'); --velours
INSERT INTO FOURNISSEURS VALUES('FOUR0004', 'Tsukuda', 'Japon', 'Chuoku', '103-0006', 'Nihombashitomizawacho', '7907900829', 'Tsukuda.japon@baidu.jp'); --laine



-- INSERT CLIENTS


-- Clients FR

INSERT INTO CLIENTS VALUES('CFR00001', 'Besson', 'Jeremy', 'France', 'Moulin', '03000', '4 rue de la patate', '0774859612','HeleneMailhot@teleworm.us');
INSERT INTO CLIENTS VALUES('CFR00002', 'Dubois', 'Patrick', 'France', 'Paris', '75000', '6 avenue du bol', '0652487896','AgricanLabbe@rhyta.com');
INSERT INTO CLIENTS VALUES('CFR00003', 'Boileau', 'Nathan', 'France', 'Marseille', '43000', '12 rue de la mangue', '0642121215','MicheleVallee@teleworm.us');
INSERT INTO CLIENTS VALUES('CFR00004', 'Lanone', 'Maxence', 'France', 'Thiers', '63300', '87 boulevard poil', '0678521010','AubertBeaule@dayrep.com');
INSERT INTO CLIENTS VALUES('CFR00005', 'Porte', 'Charles', 'France', 'Saignes', '15240', '41 boulevard la pomme', '0695642308','MalagigiBrisette@teleworm.us');
INSERT INTO CLIENTS VALUES('CFR00006', 'Laco', 'Pastop', 'France', 'Lyon', '69000', '52 rue de la lune', '0795452301','FaunaAvare@dayrep.com');
INSERT INTO CLIENTS VALUES('CFR00007', 'Douille', 'Mathis', 'France', 'Frugières_le-Pin', '43230', '70 avenue de la mer', '0689452013','JoannaRobillard@jourrapide.com');
INSERT INTO CLIENTS VALUES('CFR00008', 'Frizot', 'Colin', 'France', 'Clermont-Ferrand', '63000', '8 rue du puit', '0769977257','colin.frizot@etu.uca.fr');

-- Client UE

INSERT INTO CLIENTS VALUES('CUE00001', 'Pierron', 'Joan', 'Italie', 'Rome', '00100', 'Piazza Campo Dè Fiori, 22', '0390 9798159','joanpierron@gmail.com');
INSERT INTO CLIENTS VALUES('CUE00002', 'García', 'Abelardo', 'Espagne', 'Barcelone', '08001', 'Señora Maria Gomez Montilla', '617 320 684','AlardoEsquivelGrijalva@armyspy.com');
INSERT INTO CLIENTS VALUES('CUE00003', 'Müller', 'Emma', 'Allemagne', 'Berlin', '10115', 'Fischerinsel 69', '03723 36 20 16','HipolitoRolonNieves@dayrep.com');
INSERT INTO CLIENTS VALUES('CUE00004', 'Fischer', 'Alice', 'Allemagne', 'Munich', '80331', 'Motzstr. 86', '036373 48 30','DaraAvilaMatos@rhyta.com');
INSERT INTO CLIENTS VALUES('CUE00005', 'Costa', 'Livia', 'Italie', 'Milan', '20019', 'Pommidoro', '0890 1788749','AdemaroOzunaOrellana@jourrapide.com');
INSERT INTO CLIENTS VALUES('CUE00006', 'Fernández', 'Adela', 'Espagne', 'Madrid', '28001', 'Calle Campos Vassalo, 27', '723 263 539','KilianoValenciaRegalado@jourrapide.com');
INSERT INTO CLIENTS VALUES('CUE00007', 'Urbonas', 'Danius', 'Lituanie', 'Vilnius', '01100', 'Pebre 69', '785 4749','AntaresPedrozaAlarcon@dayrep.com');
INSERT INTO CLIENTS VALUES('CUE00008', 'Petrauskas', 'Joris', 'Lituanie', 'Kaunas', '00535', 'Kraavi 44', '451 2492','NisimPachecoTirado@teleworm.us');

-- Client Hors-UE

INSERT INTO CLIENTS VALUES('COT00001', 'Umarah', 'Murad', 'Afrique_Du_Sud', 'Le_Cap', '6665', '1091 Burger St', '082 199 4282','UmarahMuradMaloof@teleworm.us');
INSERT INTO CLIENTS VALUES('COT00002', 'Zakiy', 'Shafi', 'Afrique_Du_Sud', 'Johannesbourg', '2000', '1721 Nelson Mandela Drive', '082 957 1669','ZakiyShafiKanaan@armyspy.com ');
INSERT INTO CLIENTS VALUES('COT00003', 'Davis', 'Linsey', 'Canada', 'Ottawa', '19441', '996 James Street', '905-322-7098','LinseyCDavis@armyspy.com');
INSERT INTO CLIENTS VALUES('COT00004', 'Symonds', 'Shawn', 'Canada', 'Quebec', '23027', '4545 Weston Rd', '416-455-1694','ShawnJSymonds@teleworm.us');
INSERT INTO CLIENTS VALUES('COT00005', 'Kikuno', 'Numata', 'Japon', 'Tokyo', '100-0000', '1, Omonia Square', '95 506247','KikunoNumata@armyspy.com');
INSERT INTO CLIENTS VALUES('COT00006', 'Santtenau', 'Mattheyo', 'Mongolie', 'Hovd', '18004', '5516 Körösladány', '(30) 416-3490','metheyo.sutuno@outlook.fr');
INSERT INTO CLIENTS VALUES('COT00007', 'Alves', 'Brenda', 'Bresil', 'Rio', '20000-000', 'Rua Expedicionário Isidro Matoso, 1483', '(21) 2291-4481','BrendaAlvesCardoso@rhyta.com');
INSERT INTO CLIENTS VALUES('COT00008', 'Almeida', 'Luís', 'Manaus', 'Kaunas', '27949-316', 'Rua Um, 1128', '(22) 7187-9425','LuisAlmeidaGoncalves@jourrapide.com');

-- INSERT COMMANDE

INSERT INTO COMMANDE VALUES('CFR00001', 'CASQ0001', 'COM00001', '900', '9', '20-OCT-2021');
INSERT INTO COMMANDE VALUES('CFR00002', 'CASQ0002', 'COM00002', '100', '1', '21-OCT-2021');
INSERT INTO COMMANDE VALUES('CFR00002', 'CASQ0005', 'COM00003', '240', '2', '22-OCT-2021');
INSERT INTO COMMANDE VALUES('CFR00004', 'CASQ0008', 'COM00004', '140', '1', '25-OCT-2021');

INSERT INTO COMMANDE VALUES('CFR00005', 'TSHI0002', 'COM00005', '500', '2', '14-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00006', 'TSHI0007', 'COM00006', '260', '1', '04-NOV-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'TSHI0003', 'COM00007', '2100', '7', '04-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00008', 'TSHI0004', 'COM00008', '300', '1', '16-DEC-2021');

INSERT INTO COMMANDE VALUES('COT00001', 'PULL0001', 'COM00009', '2000', '4', '20-NOV-2021');
INSERT INTO COMMANDE VALUES('COT00001', 'PULL0002', 'COM00010', '500', '1', '15-NOV-2021');
INSERT INTO COMMANDE VALUES('COT00003', 'PULL0003', 'COM00011', '1650', '3', '04-NOV-2021');
INSERT INTO COMMANDE VALUES('COT00004', 'PULL0008', 'COM00012', '1160', '2', '10-NOV-2021');

INSERT INTO COMMANDE VALUES('COT00005', 'VEST0003', 'COM00013', '950', '1', '30-NOV-2021');
INSERT INTO COMMANDE VALUES('COT00006', 'VEST0001', 'COM00014', '800', '1', '29-NOV-2021');
INSERT INTO COMMANDE VALUES('COT00007', 'VEST0008', 'COM00015', '1598', '2', '01-DEC-2021');
INSERT INTO COMMANDE VALUES('COT00008', 'VEST0004', 'COM00016', '950', '1', '02-DEC-2021');

INSERT INTO COMMANDE VALUES('CUE00001', 'MASQ0006', 'COM00017', '2500', '50', '02-DEC-2021');
INSERT INTO COMMANDE VALUES('CUE00002', 'MASQ0004', 'COM00018', '1500', '30', '01-DEC-2021');
INSERT INTO COMMANDE VALUES('CUE00003', 'MASQ0005', 'COM00019', '1000', '20', '29-NOV-2021');
INSERT INTO COMMANDE VALUES('CUE00004', 'MASQ0001', 'COM00020', '1125', '15', '30-NOV-2021');

INSERT INTO COMMANDE VALUES('CUE00005', 'BONN0001', 'COM00021', '450', '3', '10-DEC-2021');
INSERT INTO COMMANDE VALUES('CUE00005', 'BONN0005', 'COM00022', '120', '1', '09-DEC-2021');
INSERT INTO COMMANDE VALUES('CUE00007', 'BONN0006', 'COM00023', '360', '3', '10-DEC-2021');
INSERT INTO COMMANDE VALUES('CUE00008', 'BONN0007', 'COM00024', '340', '2', '08-DEC-2021');

INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00025', '2250', '3', '29-NOV-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00026', '2250', '3', '30-NOV-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00027', '2250', '3', '01-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00028', '2250', '3', '02-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00029', '2250', '3', '03-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00030', '2250', '3', '07-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00031', '2250', '3', '08-DEC-2021');
INSERT INTO COMMANDE VALUES('CUE00004', 'PANT0001', 'COM00032', '1380', '2', '08-DEC-2021');
INSERT INTO COMMANDE VALUES('COT00003', 'PANT0002', 'COM00033', '2070', '3', '09-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00002', 'PANT0006', 'COM00034', '600', '1', '10-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00035', '2250', '3', '10-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00036', '2250', '3', '10-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00037', '2250', '3', '10-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00001', 'PANT0004', 'COM00038', '2250', '3', '11-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00039', '2250', '3', '11-DEC-2021');
INSERT INTO COMMANDE VALUES('CFR00007', 'PANT0004', 'COM00040', '2250', '3', '12-DEC-2021');


-- INSERT FOURNIR


-- INSERT FOURNIR CASQUETTE

INSERT INTO FOURNIR VALUES('CASQ0001','FOUR0002',20,100);
INSERT INTO FOURNIR VALUES('CASQ0002','FOUR0002',20,100);
INSERT INTO FOURNIR VALUES('CASQ0003','FOUR0003',40,100);
INSERT INTO FOURNIR VALUES('CASQ0004','FOUR0003',40,100);
INSERT INTO FOURNIR VALUES('CASQ0005','FOUR0001',25,100);
INSERT INTO FOURNIR VALUES('CASQ0006','FOUR0001',25,100);
INSERT INTO FOURNIR VALUES('CASQ0007','FOUR0004',30,100);
INSERT INTO FOURNIR VALUES('CASQ0008','FOUR0004',30,100);

--INSERT FOURNIR TSHIRT

INSERT INTO FOURNIR VALUES('TSHI0001','FOUR0002',60,100);
INSERT INTO FOURNIR VALUES('TSHI0002','FOUR0002',60,100);
INSERT INTO FOURNIR VALUES('TSHI0003','FOUR0003',70,100);
INSERT INTO FOURNIR VALUES('TSHI0004','FOUR0003',70,100);
INSERT INTO FOURNIR VALUES('TSHI0005','FOUR0001',68,100);
INSERT INTO FOURNIR VALUES('TSHI0006','FOUR0001',68,100);
INSERT INTO FOURNIR VALUES('TSHI0007','FOUR0004',65,100);
INSERT INTO FOURNIR VALUES('TSHI0008','FOUR0004',65,100);

--INSERT FOURNIR PULL

INSERT INTO FOURNIR VALUES('PULL0001','FOUR0002',100,100);
INSERT INTO FOURNIR VALUES('PULL0002','FOUR0002',100,100);
INSERT INTO FOURNIR VALUES('PULL0003','FOUR0003',120,100);
INSERT INTO FOURNIR VALUES('PULL0004','FOUR0003',120,100);
INSERT INTO FOURNIR VALUES('PULL0005','FOUR0001',110,100);
INSERT INTO FOURNIR VALUES('PULL0006','FOUR0001',110,100);
INSERT INTO FOURNIR VALUES('PULL0007','FOUR0004',130,100);
INSERT INTO FOURNIR VALUES('PULL0008','FOUR0004',130,100);

--INSERT FOURNIR VESTE

INSERT INTO FOURNIR VALUES('VEST0001','FOUR0001',250,100);
INSERT INTO FOURNIR VALUES('VEST0002','FOUR0001',250,100);
INSERT INTO FOURNIR VALUES('VEST0003','FOUR0003',300,100);
INSERT INTO FOURNIR VALUES('VEST0004','FOUR0003',300,100);
INSERT INTO FOURNIR VALUES('VEST0005','FOUR0002',200,100);
INSERT INTO FOURNIR VALUES('VEST0006','FOUR0002',200,100);
INSERT INTO FOURNIR VALUES('VEST0007','FOUR0004',250,100);
INSERT INTO FOURNIR VALUES('VEST0008','FOUR0004',250,100);

--INSERT FOURNIR MASQUE

INSERT INTO FOURNIR VALUES('MASQ0001','FOUR0001',10,100);
INSERT INTO FOURNIR VALUES('MASQ0002','FOUR0001',10,100);
INSERT INTO FOURNIR VALUES('MASQ0003','FOUR0003',10,100);
INSERT INTO FOURNIR VALUES('MASQ0004','FOUR0003',10,100);
INSERT INTO FOURNIR VALUES('MASQ0005','FOUR0002',10,100);
INSERT INTO FOURNIR VALUES('MASQ0006','FOUR0002',10,100);
INSERT INTO FOURNIR VALUES('MASQ0007','FOUR0004',10,100);
INSERT INTO FOURNIR VALUES('MASQ0008','FOUR0004',10,100);

--INSERT FOURNIR BONNET

INSERT INTO FOURNIR VALUES('BONN0001','FOUR0001',20,100);
INSERT INTO FOURNIR VALUES('BONN0002','FOUR0001',20,100);
INSERT INTO FOURNIR VALUES('BONN0003','FOUR0003',25,100);
INSERT INTO FOURNIR VALUES('BONN0004','FOUR0003',25,100);
INSERT INTO FOURNIR VALUES('BONN0005','FOUR0002',15,100);
INSERT INTO FOURNIR VALUES('BONN0006','FOUR0002',15,100);
INSERT INTO FOURNIR VALUES('BONN0007','FOUR0004',25,100);
INSERT INTO FOURNIR VALUES('BONN0008','FOUR0004',25,100);

--INSERT FOURNIR PANTALONS

INSERT INTO FOURNIR VALUES('PANT0001','FOUR0001',120,100);
INSERT INTO FOURNIR VALUES('PANT0002','FOUR0001',120,100);
INSERT INTO FOURNIR VALUES('PANT0003','FOUR0003',130,100);
INSERT INTO FOURNIR VALUES('PANT0004','FOUR0003',130,100);
INSERT INTO FOURNIR VALUES('PANT0005','FOUR0002',100,100);
INSERT INTO FOURNIR VALUES('PANT0006','FOUR0002',100,100);
INSERT INTO FOURNIR VALUES('PANT0007','FOUR0004',125,100);
INSERT INTO FOURNIR VALUES('PANT0008','FOUR0004',125,100);



-- 1- Y a-t-il des articles jamais commandés ?

SELECT (nom, numProd)Nom_____Numéro FROM PRODUITS 
WHERE PRODUITS.numProd NOT IN(SELECT COMMANDE.numProd FROM COMMANDE);




-- 2- Une action commercial a été menée la semaine dernière sur les clients auvergants.
-- 	  L'entreprise souhaiterait savoir s'il y a eu un effet sur les commandes. 

SELECT ((count(DISTINCT c2.*))-(count(DISTINCT c1.*)))Différence_de_ventes_avec_promo_Auverge
FROM COMMANDE c1, COMMANDE c2, CLIENTS cl 
WHERE c1.numClient = cl.numClient 
AND c2.numClient = cl.numClient 
AND cl.pays = 'France' 
AND cl.codePostal=ANY(
	SELECT DISTINCT codePostal 
	FROM CLIENTS 
	WHERE codePostal LIKE '63%' 
	OR codePostal LIKE '03%' 
	OR codePostal LIKE '15%' 	
	OR codePostal LIKE '43%') 
AND c2.dateCommande IN(
	SELECT c.dateCommande 
	FROM COMMANDE c 
	WHERE c.dateCommande>CURRENT_DATE-7
	AND c.dateCommande<=CURRENT_DATE)
AND c1.dateCommande IN(
	SELECT c.dateCommande 
	FROM COMMANDE c 
	WHERE c.dateCommande<CURRENT_DATE-7 
	AND c.dateCommande>CURRENT_DATE-14);







-- 3- Existe-t-il des clients qui n'ont jamais passé de commande ?

SELECT (nom, numClient)Nom_et_Num FROM CLIENTS c
WHERE c.numClient NOT IN(SELECT COMMANDE.numClient FROM COMMANDE);






-- 4- Total des ventes en France

SELECT sum(prix_total)total_FR FROM COMMANDE
WHERE numClient LIKE 'CFR%'
AND date_trunc('month',dateCommande) = date_trunc('month', (date_trunc('month',CURRENT_DATE)::date-1));

-- 4- Total des ventes en Europe

SELECT sum(prix_total)total_UE FROM COMMANDE
WHERE numClient LIKE 'CUE%'
OR numClient LIKE 'CFR%'
AND date_trunc('month',dateCommande) = date_trunc('month', (date_trunc('month',CURRENT_DATE)::date-1));

SELECT sum(prix_total)total_UE_Hors_France FROM COMMANDE
WHERE numClient LIKE 'CUE%'
AND date_trunc('month',dateCommande) = date_trunc('month', (date_trunc('month',CURRENT_DATE)::date-1));


-- 4- Total des ventes Hors Europe 

SELECT sum(prix_total)total_Hors_UE FROM COMMANDE
WHERE numClient LIKE 'COT%'
AND date_trunc('month',dateCommande) = date_trunc('month', (date_trunc('month',CURRENT_DATE)::date-1));
